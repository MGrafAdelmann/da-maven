package com.mycompany.app;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
		// Objekte erzeugen und durch Konstruktoraufrufe initialisieren.
		LimitedAccount a = new LimitedAccount("Hans Maier", 500);
		Account b = new Account("Fritz Mueller");
		// Methoden auf Objekten aufrufen.
		a.deposit(1000);
		a.transfer(300, b);
		for(int i = 0; i < 100; i++)
		{
			a.deposit(100 + i);
			a.transfer(50, b);
		}
		b.transfer(100, a);
		a.withdraw(2000);
		b.withdraw(2000);
		// Ausgabe.
		System.out.println("Konto a: " + a.number() + " " +
		a.holder() + " " + a.balance());
		System.out.println("Konto b: " + b.number() + " " +
		b.holder() + " " + b.balance());
    }
}
