package com.mycompany.app;
// Klasse: Konto.
class Account {
	// Objektvariablen:
	private int number; // Kontonummer.
	private String holder; // Kontoinhaber.
	private int balance; // Kontostand.
	// Klassenvariable: Nächste zu vergebende Kontonummer.
	private static int nextNumber = 1;
	// Konstruktor:
	// Konto mit Inhaber h, eindeutiger Nummer
	// und Anfangsbetrag 0 konstruieren.
	public Account (String h) {
		this.number = nextNumber++;
		this.holder = h;
		this.balance = 0;
	}
	// Objektmethoden: Kontonummer/−inhaber/−stand abfragen.
	public int number () {
		return this.number;
	}
	public String holder () {
		return this.holder;
	}
	public int balance () {
		return this.balance;
	}
	// Objektmethoden: Betrag amount einzahlen/abheben/überweisen.
	public void deposit (int amount) {
		this.balance += amount;
	}
	public void withdraw (int amount) {
		this.balance = this.balance - amount;
	}
	public void transfer (int amount, Account that) {
		this.withdraw(amount);
		that.deposit(amount);
	}
}
class LimitedAccount extends Account {
	// Zusätzliche Objektvariable:
	private int limit; // Kreditlinie in Cent.
	// Konstruktor:
	// Limitiertes Konto mit Inhaber h, Kreditlinie l,
	// eindeutiger Nummer und Anfangsbetrag 0 konstruieren.
	public LimitedAccount (String h, int l) {
	// Konstruktor der Oberklasse Account aufrufen,
	// um deren Objektvariablen zu initialisieren.
	super(h);
	limit = l;
	}
	// Zusätzliche Objektmethode: Kreditlinie abfragen.
	public int limit () { return limit; }
}